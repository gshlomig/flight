package com.flight.helpers;

import java.util.HashSet;
import java.util.Set;

import com.flight.db.TicketProfile;
import com.flight.db.TicketsDB;
import com.flight.enums.Countries;

public class CheckinService {

	private Set<Integer> flightsAvailability = new HashSet<>();

	public CheckinService() {
		initFlights();
	}

	public void initFlights() {
		for (Countries c : Countries.values()) {
			flightsAvailability.add(c.countryCode());
		}
	}

	public TicketProfile checkIn(Integer countryId, String bagId) {
		TicketProfile profile = null;
		if (flightsAvailability.contains(countryId)) {
			profile = TicketsDB.createTicket(bagId);
		}
		return profile;
	}

}
