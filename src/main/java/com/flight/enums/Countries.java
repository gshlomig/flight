package com.flight.enums;

public enum Countries {
	ISRAEL(1), ARGENTINA(2), BRAZIL(3);

	private int countryCode;

	private Countries(int countryCode) {
		this.countryCode = countryCode;
	}

	public int countryCode() {
		return this.countryCode;
	}
}
