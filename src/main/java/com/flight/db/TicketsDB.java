package com.flight.db;

import java.util.HashMap;
import java.util.Map;

public class TicketsDB {
	private static Map<Long, TicketProfile> ticketsMap = new HashMap<>();
	private static Long ticketId = 0L;

	public static TicketProfile getTicket(Long id) {
		return ticketsMap.get(id);
	}

	// TODO better locking
	public static synchronized TicketProfile createTicket(String bagId) {
		TicketProfile userData = new TicketProfile(bagId, ++ticketId);
		ticketsMap.put(ticketId, userData);
		return userData;
	}

	public static boolean hasTicket(Long id) {
		return ticketsMap.containsKey(id);
	}
}
