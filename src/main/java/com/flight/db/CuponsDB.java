package com.flight.db;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class CuponsDB {

	private Map<Integer, Double> cuponsList = new HashMap<>();
	private Random rand = new Random();
	private final int MIN_CUPON_ID = 1;
	private final int MAX_CUPON_ID = 1000;
	private final int NUM_OF_CUPONS = 1000;
	private final double[] discounts = { 0.1, 0.5, 0.6 };

	public CuponsDB() {
		for (int i = 0; i < NUM_OF_CUPONS; i++) {
			addCupon();
		}

	}

	private void addCupon() {
		int cuponId = this.rand.nextInt((MAX_CUPON_ID - MIN_CUPON_ID) + 1) + MIN_CUPON_ID;
		double discount = this.rand.nextInt(discounts.length);
		cuponsList.put(cuponId, discount);
	}

	public Double getDiscount(int cuponId) {
		return cuponsList.get(cuponId);
	}

}
