package com.flight.db;

public class TicketProfile {

	private String bagId;
	private Long ticketId;

	public TicketProfile(String bagId, Long ticketId) {
		this.bagId = bagId;
		this.ticketId = ticketId;
	}

	public String getBagId() {
		return bagId;
	}

	public void setBagId(String bagId) {
		this.bagId = bagId;
	}

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

}
