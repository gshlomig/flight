package com.flight.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flight.db.CuponsDB;
import com.flight.db.TicketProfile;
import com.flight.db.TicketsDB;
import com.flight.helpers.CheckinService;

@RestController
@RequestMapping("/flight")
public class Controller {
	private static final Logger log = LogManager.getLogger(Controller.class);
	private CheckinService checkinService = new CheckinService();
	private CuponsDB cuponDb = new CuponsDB();

	@GetMapping("ticket/{ticketId}")
	public String hasTicket(@PathVariable("ticketId") final Long ticketId) {
		log.info("Received ticket: " + ticketId);
		boolean hasTicket = TicketsDB.hasTicket(ticketId);
		if (hasTicket) {
			return "Ticket found";
		} else {
			return "No ticket found";
		}
	}

	@GetMapping("checkin/{country}/{baggage}")
	public String checkin(@PathVariable("country") final Integer countryCode,
			@PathVariable("baggage") final String bagId) {
		log.info("Received countryCode: " + countryCode + " and bagId: " + bagId);
		TicketProfile profile = checkinService.checkIn(countryCode, bagId);
		if (profile != null) {
			return "Checkin successfull, Your ticket id is: " + profile.getTicketId();
		} else {
			return "Unable to checking, please try again.";
		}
	}

	
	//Not finished
	//@GetMapping("cupon/{cuponId}/{payment}")
	public String cupon(@PathVariable("cuponId") final Integer cuponId, @PathVariable("payment") final Double payment) {
		log.info("Received cuponId: " + cuponId + " and payment: " + payment);
		Double discount = cuponDb.getDiscount(cuponId);
		if (discount != null) {
			return "Charged with discount. Total charge: " + discount * payment;
		} else {
			return "Charged without discount. Total charge : " + payment;
		}
	}

}
